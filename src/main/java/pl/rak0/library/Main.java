package pl.rak0.library;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pl.rak0.library.h2database.Database;


public class Main extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource("/window/mainWindow.fxml"));
    primaryStage.getIcons().add(new Image("/icons/library.png"));
    primaryStage.setTitle("Library");
    primaryStage.setScene(new Scene(root));
    primaryStage.show();
    primaryStage.setMinHeight(540);
    primaryStage.setMinWidth(900);
    primaryStage.setOnCloseRequest(e -> {
      e.consume();
      Database.entityManagerFactory.close();
      primaryStage.close();
    });
  }
}