package pl.rak0.library.view.controller.book;

import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.alert.AlertYesOrNo;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Book;
import pl.rak0.library.utils.Genre;
import pl.rak0.library.window.add.book.AddBook;
import pl.rak0.library.window.information.book.InformationBook;
import pl.rak0.library.window.modify.book.ModifyBook;

public class BookController {

  SortedList<Book> sortedList;
  FilteredList<Book> filteredData;

  @FXML
  private TextField searchTextField;

  @FXML
  private ChoiceBox showChoiceBox;

  @FXML
  private ChoiceBox typeChoiceBox;

  @FXML
  private ChoiceBox authorChoiceBox;

  @FXML
  private Button deleteBookButton;

  @FXML
  private Button addBookButton;

  @FXML
  private Button clearType;

  @FXML
  private Button clearAuthor;

  @FXML
  private Button rentGiveButton;

  @FXML
  private Button informationBookButton;

  @FXML
  private Button modifyBookButton;

  @FXML
  private TableView<Book> booksTable;

  @FXML
  private javafx.scene.control.TableColumn<Book, String> nameColumn;

  @FXML
  private javafx.scene.control.TableColumn<Book, String> typeColumn;

  @FXML
  private javafx.scene.control.TableColumn<Book, String> authorColumn;

  @FXML
  private javafx.scene.control.TableColumn<Book, String> rentedColumn;


  public void initialize() {
    informationBookButton.setOnAction(e -> {
      try {
        if (!booksTable.getSelectionModel().isEmpty()) {
          informationBookWindow(booksTable.getSelectionModel().getSelectedItem());
        }
      } catch (Exception ev) {
        System.out.println(ev.getMessage());
      }
    });
    rentGiveButton.setOnAction(e -> {
      try {
        if (!booksTable.getSelectionModel().isEmpty()) {
          setRentOrGive(booksTable.getSelectionModel().getSelectedItem());
        }
      } catch (Exception ev) {
        System.out.println(ev.getMessage());
      }
    });
    modifyBookButton.setOnAction(e -> {
      try {
        if (!booksTable.getSelectionModel().isEmpty()) {
          modifyBookWindow(booksTable.getSelectionModel().getSelectedItem());
        }
      } catch (Exception ev) {
        System.out.println(ev.getMessage());
      }
    });
    booksTable.setPlaceholder(new Label("No results"));
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
    authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
    rentedColumn.setCellValueFactory(new PropertyValueFactory<>("rent"));
    rentedColumn.setCellValueFactory(features -> {
      Book book = features.getValue();
      String status = Optional.ofNullable(book)
          .filter(Book::isRent)
          .map(isRent -> "Yes")
          .orElse("No");

      return new SimpleStringProperty(status);
    });

    refreshFilter();
    clearType.setOnAction(e -> typeChoiceBox.getSelectionModel().clearSelection());
    clearAuthor.setOnAction(e -> authorChoiceBox.getSelectionModel().clearSelection());
    deleteBookButton.setOnAction(e -> deleteBookAlert());
    addBookButton.setOnAction(e -> addBookWindow());
    searchTextField.textProperty()
        .addListener((observable, oldValue, newValue) -> filteredData.setPredicate(book -> {
          if (newValue == null || newValue.isEmpty() || oldValue == null) {
            return true;
          }
          String loweCaseFilter = newValue.toLowerCase();
          return book.getName().toLowerCase().contains(loweCaseFilter);
        }));
    authorChoiceBox.setOnAction((event -> filteredData.setPredicate(book -> {
          if (authorChoiceBox.getSelectionModel().isEmpty()) {
            return true;
          }
          return book.getAuthor().equals(authorChoiceBox.getValue());
        }
    )));
    typeChoiceBox.setOnAction((event -> filteredData.setPredicate(book -> {
          if (typeChoiceBox.getSelectionModel().isEmpty()) {
            return true;
          }
          return book.getType().equals(typeChoiceBox.getValue());
        }
    )));
    showChoiceBox.setOnAction((event -> filteredData.setPredicate(book -> {
          if (showChoiceBox.getSelectionModel().getSelectedItem().equals("All")) {
            return true;
          }
          if (showChoiceBox.getSelectionModel().getSelectedItem().equals("Rented")) {
            return book.isRent();
          }
          return !showChoiceBox.getSelectionModel().getSelectedItem().equals("Available") || !book
              .isRent();
        }
    )));

    sortedList.comparatorProperty().bind(booksTable.comparatorProperty());
    booksTable.setItems(sortedList);
  }

  private ObservableList getBooks() {
    ObservableList allBooks;
    allBooks = Database.getAllItems(Book.class);
    return allBooks;
  }

  private void refreshFilter() {
    typeChoiceBox.setItems(Genre.TYPES_OF_BOOKS);
    authorChoiceBox.setItems(Database.getAllAuthor());
    showChoiceBox.setItems(FXCollections.observableArrayList("All", "Rented", "Available"));
    showChoiceBox.getSelectionModel().select(0);
    filteredData = new FilteredList<>(getBooks());
    sortedList = new SortedList<>(filteredData);
  }

  private void refreshTableContent() {
    filteredData = new FilteredList<>(getBooks());
    sortedList = new SortedList<>(filteredData);
    booksTable.setItems(sortedList);
  }

  private void deleteBookAlert() {
    if (booksTable.getSelectionModel().getSelectedItem() != null) {
      if (AlertYesOrNo.display("Delete book", "Delete for sure?")) {
        try {
          Book book = booksTable.getSelectionModel().getSelectedItem();
          Database.deleteBook(book.getId());
          authorChoiceBox.setItems(Database.getAllAuthor());
        } catch (Exception e) {
          System.out.println(e.getMessage());
        } finally {
          refreshTableContent();
          booksTable.setItems(sortedList);
        }
      }
    }
  }

  private void addBookWindow() {
    try {
      AddBook addBook = new AddBook();
      if (addBook.createBook()) {
        refreshTableContent();
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private void informationBookWindow(Book book) {
    try {
      InformationBook informationBook = new InformationBook(book);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private void modifyBookWindow(Book book) {
    try {
      if (new ModifyBook().isModify(book)) {
        refreshTableContent();
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private void setRentOrGive(Book book) {
    Database.setRent(book);
    refreshTableContent();
  }
}
