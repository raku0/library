package pl.rak0.library.view.controller.order;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.window.information.order.HistoryOrderInformation;
import pl.rak0.library.window.modify.order.CorrectionOrder;

public class OrderHistoryViewController {

  @FXML
  public TableView<Order> orderHistoryView;

  @FXML
  public TableColumn<Order, String> nameColumn, createDateColumn, receivedDateColumn, commentColumn;


  public void initialize() {
    orderHistoryView.setPlaceholder(new Label("No order in history"));
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    createDateColumn.setCellValueFactory(new PropertyValueFactory<>("createDate"));
    createDateColumn.setCellValueFactory(string -> {
      Order order = string.getValue();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      return new SimpleStringProperty(dateFormat.format(order.getCreateDate()));
    });
    receivedDateColumn.setCellValueFactory(new PropertyValueFactory<>("receivedDate"));
    receivedDateColumn.setCellValueFactory(string -> {
      Order order = string.getValue();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      return new SimpleStringProperty(dateFormat.format(order.getCreateDate()));
    });
    commentColumn.setCellValueFactory(new PropertyValueFactory<>("comment"));
    commentColumn.setCellValueFactory(commentString -> {
      Order order = commentString.getValue();
      String comment = Optional.ofNullable(order).
          filter(order1 -> order.getComment() == null || order.getComment().equals(""))
          .map(getComment -> "No comment")
          .orElse(order.getComment());
      return new SimpleStringProperty(comment);
    });
    ObservableList allOrders = FXCollections.observableArrayList();
    for (Order item : Database.getAllItems(Order.class)) {
      if (item.getReceivedDate() != null) {
        allOrders.add(item);
      }
    }
    orderHistoryView.setItems(allOrders);
  }

  @FXML
  public void informationHistoryOrder() throws IOException {
    if (orderHistoryView.getSelectionModel().getSelectedItem() != null) {
      HistoryOrderInformation historyOrderInformation = new HistoryOrderInformation(
          orderHistoryView.getSelectionModel().getSelectedItem());
    }
  }

  @FXML
  public void correctionHistoryOrder() throws IOException {
    if (orderHistoryView.getSelectionModel().getSelectedItem() != null) {
      CorrectionOrder correctionOrder=new CorrectionOrder();
      if (correctionOrder.correctOrder(orderHistoryView.getSelectionModel().getSelectedItem())){
        orderHistoryView.refresh();
      }
    }
  }

}
