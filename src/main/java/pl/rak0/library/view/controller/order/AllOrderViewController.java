package pl.rak0.library.view.controller.order;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.alert.AlertYesOrNo;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.window.add.order.AddOrder;
import pl.rak0.library.window.information.order.InformationOrder;
import pl.rak0.library.window.modify.order.ModifyOrder;
import pl.rak0.library.window.modify.order.ReceivedOrder;

public class AllOrderViewController {

  @FXML
  private TableView<Order> orderView;

  @FXML
  private TableColumn<Order, String> nameColumn, createDateColumn, plannedReceivedDateColumn,
      totalPriceColumn, commentColumn;

  @FXML
  private Button addOrderButton, deleteOrderButton, modifyOrderButton, informationOrderButton;

  public void initialize() {

    orderView.setPlaceholder(new Label("No orders"));
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    createDateColumn.setCellValueFactory(new PropertyValueFactory<>("createDate"));
    createDateColumn.setCellValueFactory(string->{
      Order order=string.getValue();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      return new SimpleStringProperty(dateFormat.format(order.getCreateDate()));
    });
    totalPriceColumn.setCellValueFactory(totalPrice -> {
      Order order = totalPrice.getValue();
      return new SimpleStringProperty(String.valueOf(order.getTotalPrice()));
    });
    plannedReceivedDateColumn
        .setCellValueFactory(new PropertyValueFactory<>("plannedReceivedDate"));
    plannedReceivedDateColumn.setCellValueFactory(string->{
      Order order=string.getValue();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      return new SimpleStringProperty(dateFormat.format(order.getPlannedReceivedDate()));
    });
    commentColumn.setCellValueFactory(commentString -> {
      Order order = commentString.getValue();
      String comment = Optional.ofNullable(order).
          filter(order1 -> order.getComment() == null||order.getComment().equals(""))
          .map(getComment -> "No comment")
          .orElse(order.getComment());
      return new SimpleStringProperty(comment);
    });
    orderView.setItems(getOrders());
  }

  private ObservableList getOrders() {
    ObservableList allOrders=FXCollections.observableArrayList();

    for (Order item: Database.getAllItems(Order.class)
    ) {
      if (item.getReceivedDate()==null)allOrders.add(item);
    }
    return allOrders;
  }

  @FXML
  private void addOrder() {
    AddOrder addOrder = new AddOrder();
    try {
      if (addOrder.newOrder()) {
        orderView.setItems(getOrders());
      }
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  @FXML
  private void deleteOrder() {
    if (orderView.getSelectionModel().getSelectedItem() != null) {
      if (AlertYesOrNo.display("Delete order", "Delete for sure?")) {
        Order order = orderView.getSelectionModel().getSelectedItem();
        Database.deleteOrder(order.getId());
        orderView.setItems(getOrders());
      }
    }
  }

  @FXML
  private void informationOrder() throws IOException {
    if (orderView.getSelectionModel().getSelectedItem() != null) {
      Order order = orderView.getSelectionModel().getSelectedItem();
      InformationOrder informationOrder = new InformationOrder(order);
    }
  }

  @FXML
  private void modifyOrder() throws IOException {
    if (orderView.getSelectionModel().getSelectedItem() != null) {
      Order order = orderView.getSelectionModel().getSelectedItem();
      ModifyOrder modifyOrder = new ModifyOrder();
      if (modifyOrder.isModify(order)) {
        orderView.refresh();
      }
    }
  }

  @FXML
  private void receivedOrder() {
    if (orderView.getSelectionModel().getSelectedItem() != null) {
      Order order = orderView.getSelectionModel().getSelectedItem();
      if (AlertYesOrNo.display("Received order", "Did you received for sure?")) {
        ReceivedOrder receivedOrder = new ReceivedOrder();
        try {
          receivedOrder.makeReceived(order);
          Database.mergeObject(order);
          orderView.setItems(getOrders());
        } catch (IOException e) {
          System.out.println(e.getMessage());
        }
      }
    }
  }
}
