package pl.rak0.library.view.controller.order;


import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import pl.rak0.library.window.add.order.AddOrder;

public class OrderViewController {

  @FXML
  private GridPane mainContent;

  @FXML
  private GridPane viewContent;

  @FXML
  private Label optionLabel;


  public void initialize() {
    BackgroundImage backgroundImage = new BackgroundImage(new Image("icons/orderhouse.png"),
        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
        BackgroundSize.DEFAULT);
    Background background=new Background(backgroundImage);
    mainContent.setBackground(background);


  }
    @FXML
    private void newOrder () throws IOException {
      AddOrder addOrder = new AddOrder();
      addOrder.newOrder();
    }

    @FXML
    private void history () {
      try {
        mainContent.setBackground(null);
        mainContent.getChildren().clear();
        mainContent.getChildren()
            .add(FXMLLoader.load(getClass().getResource("/view/historyOrderView.fxml")));
        optionLabel.setText("HISTORY");
      } catch (Exception b) {
        System.out.println(b.getMessage());
      }
    }

    @FXML
    private void orders () {
      try {
        mainContent.setBackground(null);
        mainContent.getChildren().clear();
        mainContent.getChildren()
            .add(FXMLLoader.load(getClass().getResource("/view/allOrderView.fxml")));
        optionLabel.setText("ORDERS");
      } catch (Exception b) {
        System.out.println(b.getMessage());
      }
    }

    @FXML
    private void back () {
      try {
        viewContent.getChildren().clear();
        viewContent.getChildren()
            .add(FXMLLoader.load(getClass().getResource("/view/orderView.fxml")));
      } catch (Exception b) {
        System.out.println(b.getMessage());
      }
    }
  }


