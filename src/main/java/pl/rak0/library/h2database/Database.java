package pl.rak0.library.h2database;

import com.sun.istack.internal.NotNull;
import java.sql.SQLException;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.h2.tools.Server;
import pl.rak0.library.h2database.tables.Book;


public class Database {

  public static final EntityManagerFactory entityManagerFactory = Persistence
      .createEntityManagerFactory("h2");

  public static void main(String[] args) throws SQLException {
    Server.createTcpServer(args).start();
  }

  public static void entityPersist(Object object) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    entityManager.persist(object);
    entityManager.getTransaction().commit();
    entityManager.close();
  }

  public static void mergeObject(Object object) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    entityManager.merge(object);
    entityManager.getTransaction().commit();
    entityManager.close();
  }

  public static void setRent(@NotNull Book book) {
    if (book.isRent()) {
      book.setRent(false);
    } else {
      book.setRent(true);
      Date date = new Date();
      book.setRentDate(date);
    }
    mergeObject(book);
  }

  public static <T> ObservableList<T> getAllItems(Class<T> entityClass) {
    ObservableList<T> allItems = FXCollections.observableArrayList();
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    String queryWithEntityName =
        "from " + entityManager.getMetamodel().entity(entityClass).getName();
    try {
      TypedQuery<T> query = entityManager.createQuery(queryWithEntityName, entityClass);
      allItems.addAll(query.getResultList());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      entityManager.close();
    }
    return allItems;
  }

  public static <T> T getSingleItem(Class<T> entityClass, Object primaryKey) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    T object = null;
    try {
      object = entityManager.find(entityClass, primaryKey);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      entityManager.close();
    }
    return object;
  }

  public static ObservableList<String> getAllAuthor() {
    ObservableList<String> allAuthors = FXCollections.observableArrayList();
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {
      TypedQuery<String> query = entityManager
          .createQuery("select b.author from uniqueBooks b", String.class);
      allAuthors.addAll(query.getResultList());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      entityManager.close();
    }
    return allAuthors;
  }

  public static ObservableList<String> getAllNames() {
    ObservableList<String> allNames = FXCollections.observableArrayList();
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {
      TypedQuery<String> query = entityManager
          .createQuery("select b.name from uniqueBooks b", String.class);
      allNames.addAll(query.getResultList());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      entityManager.close();
    }
    return allNames;
  }

  public static Integer getUniqueBookIdByName(String nameBook) {
    Integer id = null;
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {
      TypedQuery<Integer> query = entityManager
          .createQuery("select b.id from uniqueBooks b where name=:name", Integer.class);
      query.setParameter("name", nameBook);
      id = query.getSingleResult();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      entityManager.close();
    }
    return id;
  }

  public static Integer getBookSellerIdByName(String sellerName) {
    Integer id = null;
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {
      TypedQuery<Integer> query = entityManager
          .createQuery("select b.id from sellerBooks b where name=:name", Integer.class);
      query.setParameter("name", sellerName);
      id = query.getSingleResult();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
      entityManager.close();
    }
    return id;
  }

  public static void deleteBook(Integer id) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    deleteItemFromDB(id, entityManager, "delete from books where id = :id");
  }

  private static void deleteItemFromDB(Integer id, EntityManager entityManager, String s) {
    try {
      Query query = entityManager.createQuery(s);
      query.setParameter("id", id);
      query.executeUpdate();
      entityManager.getTransaction().commit();
    } catch (Exception e) {
      System.out.println(e.getMessage());
      System.out.println(s);
    } finally {
      entityManager.close();
    }
  }

  public static void deleteOrder(Integer id) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    deleteItemFromDB(id, entityManager, "delete from orders where id = :id");
  }

}