package pl.rak0.library.h2database.tables;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import pl.rak0.library.h2database.Database;

@Entity(name = "orders")
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(nullable = false)
  private String name;

  @ManyToOne
  @JoinColumn(name = "seller_id")
  private BookSeller seller;

  @Column(nullable = false)
  private Date createDate;

  private Date plannedReceivedDate;

  private Date receivedDate;

  private String comment;


  @OneToMany(targetEntity = PriceAmountBook.class,fetch = FetchType.EAGER)
  private List<PriceAmountBook> priceAmountBook;

  @OneToMany(fetch = FetchType.EAGER,targetEntity = OrderCorrectionHistory.class,cascade = CascadeType.ALL)
  @Fetch(value = FetchMode.SELECT)
  private List<OrderCorrectionHistory> orderCorrectionHistories;


  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BookSeller getSeller() {
    return seller;
  }

  public void setSeller(BookSeller seller) {
    this.seller = seller;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void deleteBook(Integer bookID){
    for (PriceAmountBook item: priceAmountBook
    ) {
      if (item.getId().equals(bookID)){
        this.priceAmountBook.remove(item);
        Database.mergeObject(this);
      }
    }
  }

  public void addCorrection(OrderCorrectionHistory history){
    orderCorrectionHistories.add(history);
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getPlannedReceivedDate() {
    return plannedReceivedDate;
  }

  public void setPlannedReceivedDate(Date plannedReceivedDate) {
    this.plannedReceivedDate = plannedReceivedDate;
  }


  public Date getReceivedDate() {
    return receivedDate;
  }

  public void setReceivedDate(Date receivedDate) {
    this.receivedDate = receivedDate;
  }

  public List<PriceAmountBook> getPriceAmountBook() {
    return priceAmountBook;
  }

  public void setPriceAmountBook(
      List<PriceAmountBook> priceAmountBook) {
    this.priceAmountBook = priceAmountBook;
  }

  public List<OrderCorrectionHistory> getOrderCorrectionHistories() {
    return orderCorrectionHistories;
  }

  public void setOrderCorrectionHistories(
      List<OrderCorrectionHistory> orderCorrectionHistories) {
    this.orderCorrectionHistories = orderCorrectionHistories;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Double getTotalPrice() {
    Double totalPrice = 0.0;
    try {
      for (PriceAmountBook price : priceAmountBook
          ) {
        totalPrice += price.getPrice() * price.getAmount();
      }
    }catch (Exception e){
      System.out.println(e.getMessage());
    }
    return totalPrice;
  }
  public Integer getTotalAmount(){
    Integer totalAmount=0;
    try {
      for (PriceAmountBook amount: priceAmountBook){
        totalAmount+=amount.getAmount();
      }

    }catch (Exception e){
      System.out.println(e.getMessage());
    }
    return totalAmount;
  }
  public String getSellerName(){return getSeller().getName();}
}

