package pl.rak0.library.h2database.tables;


import java.util.Date;
import javafx.collections.ObservableList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import pl.rak0.library.h2database.Database;

@Entity(name = "books")
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "uniquebook_id")
  private UniqueBook uniqueBook;

  @Column(nullable = false)
  private Boolean rent;

  @Column(nullable = false)
  private Integer available;

  private Date rentDate;

  public Book() {
  }

  public Book(UniqueBook uniqueBook, Integer available) {
    setUniqueBook(uniqueBook);
    setAvailable(available);
    setRent(false);
  }

  @PostPersist
  public void checkAndAddUniqueBook() {
    ObservableList<UniqueBook> allUnique;
    allUnique = Database.getAllItems(UniqueBook.class);
    boolean find = false;
    Integer id_unique = null;
    for (UniqueBook book : allUnique
        ) {
      if (this.getName().equals(book.getName())) {
        if (this.getAuthor().equals(book.getAuthor())) {
          find = true;
          id_unique = book.getId();
        }
      }
    }
    if (!find) {
      UniqueBook uniqueBook = new UniqueBook();
      uniqueBook.setName(this.getName());
      uniqueBook.setAuthor(this.getAuthor());
      uniqueBook.setDescription(this.getDescription());
      uniqueBook.setNumberOfPages(this.getNumberOfPages());
      uniqueBook.setType(this.getType());
      uniqueBook.setReleaseDate(this.getReleaseDate());
      Database.entityPersist(uniqueBook);
      this.setUniqueBook(uniqueBook);
    } else {
      this.setUniqueBook(Database.getSingleItem(UniqueBook.class, id_unique));
    }
  }

  public UniqueBook getUniqueBook() {
    return uniqueBook;
  }

  public void setUniqueBook(UniqueBook uniqueBook) {
    this.uniqueBook = uniqueBook;
  }

  public Integer getId() {
    return id;
  }


  public String getName() {
    return uniqueBook.getName();
  }

  public Date getRentDate() {
    return rentDate;
  }

  public void setRentDate(Date rentDate) {
    this.rentDate = rentDate;
  }

  public String getType() {
    return uniqueBook.getType();
  }

  public String getAuthor() {
    return uniqueBook.getAuthor();
  }

  public int getReleaseDate() {
    return uniqueBook.getReleaseDate();
  }


  public int getNumberOfPages() {
    return uniqueBook.getNumberOfPages();
  }

  public String getDescription() {
    return uniqueBook.getDescription();
  }

  public boolean isRent() {
    return rent;
  }

  public void setRent(Boolean rent) {
    this.rent = rent;
  }

  public Integer getAvailable() {
    return available;
  }

  public void setAvailable(Integer available) {
    this.available = available;
  }

}
