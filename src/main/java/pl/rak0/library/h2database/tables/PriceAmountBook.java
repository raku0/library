package pl.rak0.library.h2database.tables;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity(name = "price_amount")
public class PriceAmountBook {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private Double price, priceTotal;

  private Integer amount;

  private Boolean received;

  @ManyToOne
  private UniqueBook uniqueBook;


  public Integer getId() {
    return id;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public UniqueBook getUniqueBook() {
    return uniqueBook;
  }

  public void setUniqueBook(UniqueBook uniqueBook) {
    this.uniqueBook = uniqueBook;
  }

  public Boolean getReceived() {
    return received;
  }

  public void setReceived(Boolean received) {
    this.received = received;
  }

  //Unique books info
  public String getName() {
    return uniqueBook.getName();
  }

  public String getAuthor() {
    return uniqueBook.getAuthor();
  }

  public String getType() {
    return uniqueBook.getType();
  }

  public Double getPriceTotal() {
    DecimalFormat df = new DecimalFormat("#.####");
    df.setRoundingMode(RoundingMode.CEILING);
    priceTotal = (amount * price);
    return Double.parseDouble(df.format(amount * price));
  }
}
