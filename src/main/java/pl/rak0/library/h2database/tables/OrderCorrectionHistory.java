package pl.rak0.library.h2database.tables;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "order_correction_history")
public class OrderCorrectionHistory {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private Integer order_id;

  private String name;

  @ManyToOne
  @JoinColumn(name = "seller_id")
  private BookSeller seller;

  private String comment;

  @OneToMany(targetEntity = PriceAmountBook.class, fetch = FetchType.EAGER)
  private List<PriceAmountBook> priceAmountBook;

  private Date changeDate;

  private String reason;


  public Integer getOrder_id() {
    return order_id;
  }

  public void setOrder_id(Integer order_id) {
    this.order_id = order_id;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Date getChangeDate() {
    return changeDate;
  }

  public void setChangeDate(Date changeDate) {
    this.changeDate = changeDate;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BookSeller getSeller() {
    return seller;
  }

  public void setSeller(BookSeller seller) {
    this.seller = seller;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public List<PriceAmountBook> getPriceAmountBook() {
    return priceAmountBook;
  }

  public void setPriceAmountBook(
      List<PriceAmountBook> priceAmountBook) {
    this.priceAmountBook = priceAmountBook;
  }
}
