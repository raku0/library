package pl.rak0.library.window.login;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Login extends Application {


  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {

    Parent root = FXMLLoader.load(getClass().getResource("/window/loginWindow.fxml"));
    primaryStage.setTitle("Login");
    primaryStage.setScene(new Scene(root));
    primaryStage.show();
  }
}