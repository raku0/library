package pl.rak0.library.window.modify.order.item;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.window.add.order.OrderItemForCell;


public class ModifyOrderItem {

  public static Stage stage;
  public static OrderItemForCell updatedItem;

  public OrderItemForCell isModify(OrderItemForCell orderItemForCell) throws Exception {
    ModifyOrderItemController.orderItemForCell = orderItemForCell;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/modifyOrderItemWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/setting.png"));
    stage.setTitle("Modify");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return updatedItem;
  }
}
