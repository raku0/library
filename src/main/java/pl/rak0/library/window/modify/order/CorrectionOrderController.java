package pl.rak0.library.window.modify.order;


import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.exception.ConstraintViolationException;
import pl.rak0.library.alert.AlertOk;
import pl.rak0.library.alert.AlertWithComment;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.OrderCorrectionHistory;
import pl.rak0.library.h2database.tables.PriceAmountBook;
import pl.rak0.library.window.modify.order.item.ItemEdit;

public class CorrectionOrderController {

  public static Order order;

  @FXML
  public TextField nameOrderTextField, sellerNameTextField, commentTextField;

  @FXML
  public DatePicker createDate, plannedReceiveDate, receiveDate;

  @FXML
  public TableView<PriceAmountBook> orderItemTableView;

  @FXML
  public TableColumn<PriceAmountBook, String> nameItemColumn, authorItemColumn;

  @FXML
  public TableColumn<PriceAmountBook, Integer> amountItemColumn;

  @FXML
  public TableColumn<PriceAmountBook, Double> priceItemColumn;

  private Boolean sellerChange = false;
  private Boolean itemChange = false;
  private OrderCorrectionHistory orderCorrectionHistory=new OrderCorrectionHistory();


  public void initialize() {
    orderItemTableView.setPlaceholder(new Label("No items"));
    nameOrderTextField.setText(order.getName());
    sellerNameTextField.setEditable(false);
    sellerNameTextField.setText(order.getSellerName());
    commentTextField.setText(order.getComment());
    nameItemColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    authorItemColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
    amountItemColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
    priceItemColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
    orderItemTableView.getItems().addAll(order.getPriceAmountBook());
    LocalDate date = order.getCreateDate().toInstant().atZone(ZoneId.systemDefault())
        .toLocalDate();
    createDate.setValue(date);
    date = order.getReceivedDate().toInstant().atZone(ZoneId.systemDefault())
        .toLocalDate();
    receiveDate.setValue(date);
    date = order.getPlannedReceivedDate().toInstant().atZone(ZoneId.systemDefault())
        .toLocalDate();
    plannedReceiveDate.setValue(date);

  }

  @FXML
  public void modifyItem() throws IOException {
    orderCorrectionHistory.setPriceAmountBook(order.getPriceAmountBook());
    ItemEdit itemEdit = new ItemEdit();
    if (orderItemTableView.getSelectionModel().getSelectedItem() != null) {
      if (itemEdit.edit(orderItemTableView.getSelectionModel().getSelectedItem())) {
        itemChange = true;
        orderItemTableView.refresh();
      }
      else orderCorrectionHistory.setPriceAmountBook(null);
    }
  }

  @FXML
  public void save() {
    if (!nameOrderTextField.getText().equals(order.getName()) || !commentTextField.getText()
        .equals(order.getComment()) ||
        sellerChange || itemChange) {
      try {
        orderCorrectionHistory
            .setReason(AlertWithComment.display("Reason", "Write reason of change"));
        if (orderCorrectionHistory.getReason()==null||orderCorrectionHistory.getReason().trim().equals("")){
          AlertOk.display("Error","Reason cant be empty");
        }else {
          if (!itemChange)orderCorrectionHistory.setPriceAmountBook(order.getPriceAmountBook());
          orderCorrectionHistory.setChangeDate(new Date());
          orderCorrectionHistory.setComment(order.getComment());
          orderCorrectionHistory.setName(order.getName());
          orderCorrectionHistory.setSeller(order.getSeller());
          orderCorrectionHistory.setOrder_id(order.getId());
          order.addCorrection(orderCorrectionHistory);
          order.setName(nameOrderTextField.getText());
          order.setComment(commentTextField.getText());
          LocalDate localDate = receiveDate.getValue();
          Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
          Date date = Date.from(instant);
          order.setReceivedDate(date);
          localDate = plannedReceiveDate.getValue();
          instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
          date = Date.from(instant);
          order.setPlannedReceivedDate(date);
          localDate = createDate.getValue();
          instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
          date = Date.from(instant);
          order.setCreateDate(date);
        Database.mergeObject(order);
        CorrectionOrder.check = true;
        CorrectionOrder.stage.close();}
      } catch (ConstraintViolationException e) {
        System.out.println(e.getConstraintName());
      }
    }
  }

  @FXML
  public void cancel() {
    CorrectionOrder.stage.close();
  }

  @FXML
  public void changeSeller() throws IOException {
    ChangeSeller changeSeller = new ChangeSeller();
    if (changeSeller.isChanged(order)) {
      sellerNameTextField.setText(order.getSellerName());
      sellerChange = true;
    }
  }
}
