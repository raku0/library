package pl.rak0.library.window.modify.order;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;

public class ChangeSeller {

  public static Boolean changed = false;
  public static Stage stage;

  public Boolean isChanged(Order order) throws IOException {
    ChangeSellerController.order = order;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/changeSellerWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/man.png"));
    stage.setTitle("Change Seller");
    stage.setScene(new Scene(root));
    stage.setResizable(true);
    stage.showAndWait();
    return changed;
  }
}
