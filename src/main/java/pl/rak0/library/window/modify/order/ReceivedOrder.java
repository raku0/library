package pl.rak0.library.window.modify.order;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;

public class ReceivedOrder {
  public static Stage stage;

  public boolean makeReceived(Order order) throws IOException {
    ReceivedOrderController.order=order;
    stage=new Stage();
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/plus.png"));
    Parent root = FXMLLoader.load(getClass().getResource("/window/receivedOrderWindow.fxml"));
    stage.setTitle("Order "+order.getName());
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return true;
  }

}
