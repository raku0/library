package pl.rak0.library.window.modify.order;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;

public class ModifyOrder {

  public static boolean check = false;
  public static Stage stage;

  public Boolean isModify(Order order) throws IOException {
    ModifyOrderController.order=order;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/modifyOrderWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/setting.png"));
    stage.setTitle("Modify order");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return check;
  }

}
