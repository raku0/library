package pl.rak0.library.window.modify.order.item;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.controlsfx.control.textfield.TextFields;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.PriceAmountBook;
import pl.rak0.library.h2database.tables.UniqueBook;
import pl.rak0.library.utils.InputUtils;

public class ItemEditController {

  public static PriceAmountBook priceAmountBook;

  @FXML
  public TextField nameBookTextField, priceBookTextField, amountBookTextField;

  @FXML
  public Label authorBookLabel, typeBookLabel;



  public void initialize() {
    nameBookTextField.setText(priceAmountBook.getName());
    priceBookTextField.setText(priceAmountBook.getPrice().toString());
    amountBookTextField.setText(priceAmountBook.getAmount().toString());
    authorBookLabel.setText(priceAmountBook.getAuthor());
    typeBookLabel.setText(priceAmountBook.getType());
    InputUtils.addKeyTypingFilter(
        priceBookTextField,
        event -> priceBookTextField.getLength() < 8,
        keyEvent -> keyEvent.getCharacter().matches("[0-9.]")
    );
    InputUtils.addKeyTypingFilter(
        amountBookTextField,
        event -> amountBookTextField.getLength() < 8,
        keyEvent -> keyEvent.getCharacter().matches("\\d")
    );
    TextFields.bindAutoCompletion(nameBookTextField, Database.getAllNames())
        .setOnAutoCompleted(e -> {
          UniqueBook uniqueBook = Database.getSingleItem(UniqueBook.class,
              Database.getUniqueBookIdByName(nameBookTextField.getText()));
          authorBookLabel.setText(uniqueBook.getAuthor());
          typeBookLabel.setText(uniqueBook.getType());
        });
  }

  @FXML
  public void saveItem() {
    if (!nameBookTextField.equals(priceAmountBook.getName()) || !authorBookLabel.getText()
        .equals(priceAmountBook.getAuthor()) || !typeBookLabel.getText().equals(priceAmountBook.getType())
        || !amountBookTextField.toString().equals(priceAmountBook.getAmount().toString()) || !priceAmountBook
        .toString().equals(priceAmountBook.getPrice().toString())) {
      priceAmountBook.setUniqueBook(Database.getSingleItem(UniqueBook.class,
          Database.getUniqueBookIdByName(nameBookTextField.getText())));
    }
    priceAmountBook.setPrice(Double.parseDouble(priceBookTextField.getText()));
    priceAmountBook.setAmount(Integer.parseInt(amountBookTextField.getText()));
    Database.mergeObject(priceAmountBook);
    ItemEdit.changed=true;
    ItemEdit.stage.close();
  }

  @FXML
  public void cancelItem() {
    ItemEdit.stage.close();
  }

}
