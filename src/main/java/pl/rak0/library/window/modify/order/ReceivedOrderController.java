package pl.rak0.library.window.modify.order;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.alert.AlertYesOrNo;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.PriceAmountBook;
import pl.rak0.library.window.modify.order.item.ReceivedItemEdit;
import pl.rak0.library.window.modify.order.item.ReceivedItemsCheck;

public class ReceivedOrderController {

  public static Order order;
  @FXML
  private Label nameLabel;
  @FXML
  private TableView<ReceivedItemsCheck> orderItemTable;
  @FXML
  private DatePicker receivedDate;
  @FXML
  private TableColumn<ReceivedItemsCheck, String> nameColumn;
  @FXML
  private TableColumn<ReceivedItemsCheck, Boolean> receivedColumn;
  @FXML
  private TableColumn<ReceivedItemsCheck, Integer> amountColumn;

  private Boolean clicked = true;

  private ObservableList<ReceivedItemsCheck> list = FXCollections.observableArrayList();

  public void initialize() throws NullPointerException {
    orderItemTable.setPlaceholder(new Label("No books in order"));
    nameLabel.setText(nameLabel.getText() + order.getName());
    receivedDate.setValue(LocalDate.now());
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
    receivedColumn.setCellFactory(CheckBoxTableCell.forTableColumn(receivedColumn));
    receivedColumn.setCellValueFactory(c -> c.getValue().getCheck());

    for (PriceAmountBook item : order.getPriceAmountBook()
        ) {
      ReceivedItemsCheck itemsCheck = new ReceivedItemsCheck();
      itemsCheck.setPriceAmountBook(item);
      list.add(itemsCheck);
    }
    orderItemTable.setItems(list);

  }

  @FXML
  public void makeCheckAll() {
    if (clicked) {
      for (ReceivedItemsCheck item : list
          ) {
        item.setCheck(new SimpleBooleanProperty(clicked));
      }
      clicked = false;
    } else {
      for (ReceivedItemsCheck item : list
          ) {
        item.setCheck(new SimpleBooleanProperty(clicked));
      }
      clicked = true;
    }

    orderItemTable.refresh();

  }

  @FXML
  public void editOrderItem() throws IOException {
    if (orderItemTable.getSelectionModel().getSelectedItem() != null) {
      ReceivedItemEdit receivedItemEdit = new ReceivedItemEdit();
      if (receivedItemEdit.EditItem(orderItemTable.getSelectionModel().getSelectedItem())) {
        orderItemTable.refresh();
      }
    }
  }

  @FXML
  public void deleteItem() {
    ObservableList<PriceAmountBook> list2 = FXCollections.observableArrayList();
    if (orderItemTable.getSelectionModel().getSelectedItem() != null) {
      if (AlertYesOrNo.display("Delete item", "Delete for sure?")) {
        orderItemTable.getItems().remove(orderItemTable.getSelectionModel().getSelectedItem());
      }
      for (ReceivedItemsCheck item : orderItemTable.getItems()
          ) {
        list2.add(item.getPriceAmountBook());
      }
      order.setPriceAmountBook(list2);
    }
  }

  @FXML
  public void saveReceived() {
    ObservableList<PriceAmountBook> list3 = FXCollections.observableArrayList();
    try {
      if (receivedDate != null) {
        for (ReceivedItemsCheck item : orderItemTable.getItems()
            ) {
          PriceAmountBook priceAmountBook = item.getPriceAmountBook();
          priceAmountBook.setReceived(item.getCheck().getValue());
          Database.mergeObject(priceAmountBook);
          list3.add(priceAmountBook);
        }
        order.setPriceAmountBook(list3);
        LocalDate localDate1 = receivedDate.getValue();
        Instant instant1 = Instant.from(localDate1.atStartOfDay(ZoneId.systemDefault()));
        Date date1 = Date.from(instant1);
        order.setReceivedDate(date1);
        ReceivedOrder.stage.close();
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  @FXML
  public void cancelReceived() {
    ReceivedOrder.stage.close();
  }

}
