package pl.rak0.library.window.modify.order.item;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import pl.rak0.library.h2database.tables.PriceAmountBook;

public class ReceivedItemsCheck {

  private PriceAmountBook priceAmountBook;
  private BooleanProperty check = new SimpleBooleanProperty(false);


  public String getName() {
    return priceAmountBook.getName();
  }

  public PriceAmountBook getPriceAmountBook() {
    return priceAmountBook;
  }

  public Integer getAmount() {
    return priceAmountBook.getAmount();
  }

  public void setPriceAmountBook(PriceAmountBook priceAmountBook) {
    this.priceAmountBook = priceAmountBook;
  }

  public BooleanProperty getCheck() {
    return this.check;
  }

  public void setCheck(BooleanProperty check) {
    this.check = check;
  }
}
