package pl.rak0.library.window.modify.book;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Book;

public class ModifyBook {

  public static Stage stage;
  public static boolean modify = false;

  public boolean isModify(Book book) throws Exception {
    ModifyBookController.book = book;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/modifyBookWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/setting.png"));
    stage.setTitle("Modify");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return modify;
  }
}
