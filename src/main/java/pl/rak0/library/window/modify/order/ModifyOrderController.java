package pl.rak0.library.window.modify.order;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.window.modify.order.item.ShowAllOrderItem;

public class ModifyOrderController {

  public static Order order;

  @FXML
  public TextField nameOrder, sellerOrder, commentOrder;

  @FXML
  public DatePicker createDateOrder, plannedReceivedDateOrder;

  private Boolean sellerChange = false;

  public void initialize() {
    nameOrder.setText(order.getName());
    sellerOrder.setText(order.getSeller().getName());
    commentOrder.setText(order.getComment());
    LocalDate createDate = order.getCreateDate().toInstant().atZone(ZoneId.systemDefault())
        .toLocalDate();
    createDateOrder.setValue(createDate);
    LocalDate plannedReceivedDate = order.getPlannedReceivedDate().toInstant()
        .atZone(ZoneId.systemDefault()).toLocalDate();
    plannedReceivedDateOrder.setValue(plannedReceivedDate);
  }

  @FXML
  public void saveOrder() {
    if (!order.getName().equals(nameOrder.getText()) || !order.getSeller().getName()
        .equals(sellerOrder.getText()) || !order.getComment().equals(commentOrder.getText())
        || !order.getCreateDate().toInstant()
        .atZone(ZoneId.systemDefault()).toLocalDate()
        .equals(createDateOrder.getValue()) || !order.getPlannedReceivedDate().toInstant()
        .atZone(ZoneId.systemDefault()).toLocalDate().equals(plannedReceivedDateOrder.getValue())
        || sellerChange) {
      ModifyOrder.check = true;
      order.setName(nameOrder.getText());
      order.setComment(commentOrder.getText());
      LocalDate localDate = plannedReceivedDateOrder.getValue();
      Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
      Date date = Date.from(instant);
      order.setPlannedReceivedDate(date);
      LocalDate localDate2 = createDateOrder.getValue();
      Instant instant2 = Instant.from(localDate2.atStartOfDay(ZoneId.systemDefault()));
      Date date2 = Date.from(instant2);
      order.setCreateDate(date2);
      Database.mergeObject(order);
      ModifyOrder.check = true;
      ModifyOrder.stage.close();
    }
    ModifyOrder.stage.close();
  }

  @FXML
  public void cancelOrder() {
    ModifyOrder.stage.close();
  }

  @FXML
  public void editItemsOrder() throws IOException {
    ShowAllOrderItem showAllOrderItem = new ShowAllOrderItem();
    showAllOrderItem.isEdited(order);

  }

  @FXML
  public void editSellerOrder() throws IOException {
    ChangeSeller changeSeller = new ChangeSeller();
    if (changeSeller.isChanged(order)) {
      sellerChange = true;
      sellerOrder.setText(order.getSeller().getName());
    }

  }
}
