package pl.rak0.library.window.modify.order.item;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ReceivedItemEdit {

  public static Stage stage;
  public static Boolean edited = false;

  public Boolean EditItem(ReceivedItemsCheck item) throws IOException {
    ReceivedItemEditController.item = item;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/receivedOrderItemEditWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/setting.png"));
    stage.setTitle("Modify item");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return edited;
  }

}
