package pl.rak0.library.window.modify.book;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Book;
import pl.rak0.library.utils.Genre;
import pl.rak0.library.utils.InputUtils;

public class ModifyBookController {

  public static Book book;

  @FXML
  private TextField nameTextField;

  @FXML
  private TextField authorTextField;

  @FXML
  private TextField releaseDateTextField;

  @FXML
  private TextField availableTextField;

  @FXML
  private TextField numberOfPageTextField;

  @FXML
  private TextArea descriptionTextArea;

  @FXML
  private ChoiceBox typeChoiceBox;

  @FXML
  private TextField rentTextField;

  @FXML
  private TextField rentDateTextField;

  @FXML
  private Button okButton;

  public void initialize() {
    okButton.setOnAction(e -> {
      updateBook(book);
      ModifyBook.stage.close();
    });
    nameTextField.setText(book.getName());
    typeChoiceBox.setItems(Genre.TYPES_OF_BOOKS);
    typeChoiceBox.getSelectionModel().select(book.getType());
    authorTextField.setText(book.getAuthor());
    releaseDateTextField.setText(String.valueOf(book.getReleaseDate()).equals("0") ? "Unknown"
        : String.valueOf(book.getReleaseDate()));
    rentTextField.setText(book.isRent() ? "Yes" : "No");
    rentDateTextField.setText(book.getRentDate() == null ? "" : book.getRentDate().toString());
    numberOfPageTextField.setText(String.valueOf(book.getNumberOfPages()));
    availableTextField.setText(String.valueOf(book.getAvailable()));
    descriptionTextArea.setText(book.getDescription());

    InputUtils.addKeyTypingFilter(
        releaseDateTextField,
        event -> releaseDateTextField.getLength() < 4,
        event -> event.getCharacter().matches("\\d")
    );
    InputUtils.addKeyTypingFilter(
        numberOfPageTextField,
        event -> numberOfPageTextField.getLength() < 6,
        event -> event.getCharacter().matches("\\d")
    );
    InputUtils.addKeyTypingFilter(
        availableTextField,
        event -> availableTextField.getLength() < 2,
        event -> event.getCharacter().matches("\\d")
    );

  }

  private void updateBook(Book book) {
    if (book.getName().equals(nameTextField.getText())
        && book.getAuthor().equals(authorTextField.getText())
        && book.getType().equals(typeChoiceBox.getValue().toString())
        && String.valueOf(book.getReleaseDate()).equals(releaseDateTextField.getText())
        && String.valueOf(book.getAvailable()).equals(availableTextField.getText())
        && String.valueOf(book.getNumberOfPages()).equals(numberOfPageTextField.getText())
        && book.getDescription().equals(descriptionTextArea.getText())) {
    } else {
      book.getUniqueBook().setName(nameTextField.getText());
      book.getUniqueBook().setAuthor(authorTextField.getText());
      book.getUniqueBook().setType(typeChoiceBox.getValue().toString());
      book.setAvailable(Integer.parseInt(availableTextField.getText()));
      book.getUniqueBook().setDescription(descriptionTextArea.getText());
      book.getUniqueBook().setNumberOfPages(Integer.parseInt(numberOfPageTextField.getText()));
      book.getUniqueBook().setReleaseDate(Integer.parseInt(releaseDateTextField.getText()));
      try {
        Database.mergeObject(book.getUniqueBook());
        ModifyBook.modify = true;
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }
  }
}
