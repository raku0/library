package pl.rak0.library.window.modify.order;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.alert.AlertOk;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.BookSeller;
import pl.rak0.library.h2database.tables.Order;

public class ChangeSellerController {

  public static Order order;

  @FXML
  private TableView<BookSeller> sellerTableView;

  @FXML
  private TableColumn<BookSeller,String> nameColumn,companyColumn;

  @FXML
  public void initialize(){
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    companyColumn.setCellValueFactory(new PropertyValueFactory<>("company"));
    sellerTableView.setItems(Database.getAllItems(BookSeller.class));
  }

  @FXML
  public void select(){
    if (sellerTableView.getSelectionModel().getSelectedItem()!=null){
      if (order.getSeller().getId().equals(sellerTableView.getSelectionModel().getSelectedItem().getId())){
        ChangeSeller.stage.close();
      }
      else {
        ChangeSeller.changed=true;
        order.setSeller(sellerTableView.getSelectionModel().getSelectedItem());
        ChangeSeller.stage.close();
      }
    }else {
      AlertOk.display("Error","You need pick up a seller");
    }
  }

  @FXML
  public void cancel(){
    ChangeSeller.stage.close();
  }
}
