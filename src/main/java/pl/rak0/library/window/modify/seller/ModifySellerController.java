package pl.rak0.library.window.modify.seller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.BookSeller;

public class ModifySellerController {

  public static BookSeller bookSeller;

  @FXML
  private TextField nameSellerTextField, companySellerTextField;

  public void initialize() {
    nameSellerTextField.setText(bookSeller.getName());
    companySellerTextField.setText(bookSeller.getCompany());
  }

  @FXML
  public void saveSeller() {
    if (!bookSeller.getName().equals(nameSellerTextField.getText()) || !bookSeller.getCompany()
        .equals(companySellerTextField.getText())) {
      try {
        bookSeller.setName(nameSellerTextField.getText());
        bookSeller.setCompany(companySellerTextField.getText());
        Database.mergeObject(bookSeller);
        ModifySeller.check = true;
        ModifySeller.stage.close();
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }
    else ModifySeller.stage.close();
  }

  @FXML
  public void cancel() {
    ModifySeller.stage.close();
  }
}
