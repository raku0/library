package pl.rak0.library.window.modify.order.item;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;


public class ShowAllOrderItem {

  public static Stage stage;
  public static Boolean edited = false;

  public Boolean isEdited(Order order) throws IOException {
    ShowAllOrderItemController.order=order;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/showAllOrderItemWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/orders.png"));
    stage.setTitle("Order items");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return edited;
  }
}
