package pl.rak0.library.window.modify.order.item;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.controlsfx.control.textfield.TextFields;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.UniqueBook;
import pl.rak0.library.utils.InputUtils;

public class ReceivedItemEditController {

  public static ReceivedItemsCheck item;

  @FXML
  public TextField nameBookTextField, priceBookTextField, amountBookTextField;

  @FXML
  public Label authorBookLabel, typeBookLabel;

  public void initialize() {
    nameBookTextField.setText(item.getName());
    priceBookTextField.setText(item.getPriceAmountBook().getPrice().toString());
    amountBookTextField.setText(item.getAmount().toString());
    authorBookLabel.setText(item.getPriceAmountBook().getAuthor());
    typeBookLabel.setText(item.getPriceAmountBook().getType());
    InputUtils.addKeyTypingFilter(
        priceBookTextField,
        event -> priceBookTextField.getLength() < 8,
        keyEvent -> keyEvent.getCharacter().matches("[0-9.]")
    );
    InputUtils.addKeyTypingFilter(
        amountBookTextField,
        event -> amountBookTextField.getLength() < 8,
        keyEvent -> keyEvent.getCharacter().matches("\\d")
    );
    TextFields.bindAutoCompletion(nameBookTextField, Database.getAllNames())
        .setOnAutoCompleted(e -> {
          UniqueBook uniqueBook = Database.getSingleItem(UniqueBook.class,
              Database.getUniqueBookIdByName(nameBookTextField.getText()));
          authorBookLabel.setText(uniqueBook.getAuthor());
          typeBookLabel.setText(uniqueBook.getType());
        });

  }

  @FXML
  public void saveItem() {

  }

  @FXML
  public void cancelItem() {
    ReceivedItemEdit.stage.close();
  }
}
