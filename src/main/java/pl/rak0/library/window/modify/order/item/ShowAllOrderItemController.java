package pl.rak0.library.window.modify.order.item;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.PriceAmountBook;

public class ShowAllOrderItemController {

  public static Order order;

  @FXML
  private TableView<PriceAmountBook> orderItemsTableView;

  @FXML
  private TableColumn<PriceAmountBook, String> nameBook, authorBook;

  @FXML
  private TableColumn<PriceAmountBook, Integer> amountBook;

  @FXML
  private TableColumn<PriceAmountBook, Double> priceBook;

  private ObservableList<PriceAmountBook> allBooks = FXCollections.observableArrayList();

  @FXML
  public void initialize() {
    orderItemsTableView.setPlaceholder(new Label("No Items"));
    nameBook.setCellValueFactory(new PropertyValueFactory<>("name"));
    authorBook.setCellValueFactory(new PropertyValueFactory<>("author"));
    amountBook.setCellValueFactory(new PropertyValueFactory<>("amount"));
    priceBook.setCellValueFactory(new PropertyValueFactory<>("price"));
    allBooks.addAll(order.getPriceAmountBook());
    orderItemsTableView.setItems(allBooks);
  }

  @FXML
  public void editItem() {
    if (orderItemsTableView.getSelectionModel().getSelectedItem() != null) {
      ItemEdit itemEdit = new ItemEdit();
      try {
        if (itemEdit.edit(orderItemsTableView.getSelectionModel().getSelectedItem())) {
          orderItemsTableView.refresh();
        }
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }
  }

  @FXML
  public void deleteBook() {
    if (orderItemsTableView.getSelectionModel().getSelectedItem() != null) {
      orderItemsTableView.getItems()
          .remove(orderItemsTableView.getSelectionModel().getSelectedItem());
    }
    order.setPriceAmountBook(orderItemsTableView.getItems());
  }

  @FXML
  public void exit() {
    ShowAllOrderItem.stage.close();
  }

}
