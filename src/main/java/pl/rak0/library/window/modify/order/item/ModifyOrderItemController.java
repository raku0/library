package pl.rak0.library.window.modify.order.item;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import org.controlsfx.control.textfield.TextFields;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.UniqueBook;
import pl.rak0.library.utils.InputUtils;
import pl.rak0.library.window.add.order.OrderItemForCell;

public class ModifyOrderItemController {

  public static OrderItemForCell orderItemForCell;


  @FXML
  private TextField nameTextField;

  @FXML
  private TextField authorTextField;

  @FXML
  private TextField priceTextField;

  @FXML
  private TextField amountTextField;

  private String nameForCheckChange;

  private String authorForCheckChange;

  private String priceForCheckChange;

  private String amountForCheckChange;

  public void initialize() {
    TextFields.bindAutoCompletion(nameTextField, Database.getAllNames())
        .setOnAutoCompleted(
            event -> {
              nameTextField.setEditable(false);
              UniqueBook uniqueBook = Database.getSingleItem(UniqueBook.class,
                  Database.getUniqueBookIdByName(nameTextField.getText()));
              authorTextField.setText(uniqueBook.getAuthor());
            });
    InputUtils.addKeyTypingFilter(
        priceTextField,
        keyEvent -> keyEvent.getCharacter().matches("[0-9.]")
    );
    InputUtils.addKeyTypingFilter(
        amountTextField,
        keyEvent -> amountTextField.getLength() < 4,
        keyEvent -> keyEvent.getCharacter().matches("\\d")
    );
    nameTextField.setText(orderItemForCell.getName());
    nameForCheckChange = nameTextField.getText();
    authorTextField.setText(orderItemForCell.getUniqueBook().getAuthor());
    authorForCheckChange = authorTextField.getText();
    priceTextField.setText(orderItemForCell.getPriceTotal().toString());
    priceForCheckChange = priceTextField.getText();
    amountTextField.setText(orderItemForCell.getAmount().toString());
    amountForCheckChange = amountTextField.getText();
  }

  @FXML
  public void cancelButton() {
    ModifyOrderItem.stage.close();
  }

  @FXML
  public void saveButton() {
    if (nameTextField.toString().equals(nameForCheckChange) && authorTextField.toString()
        .equals(authorForCheckChange) && priceTextField.toString().equals(priceForCheckChange)
        && amountTextField.toString().equals(amountForCheckChange) ) {
      ModifyOrderItem.stage.close();
    } else {
      OrderItemForCell orderItemForCell = new OrderItemForCell();
      orderItemForCell.setUniqueBook(Database.getUniqueBookIdByName(nameTextField.getText()));
      orderItemForCell.setAmount(Integer.parseInt(amountTextField.getText()));
      orderItemForCell.setPriceEach(Double.parseDouble(priceTextField.getText()));
      ModifyOrderItem.updatedItem = orderItemForCell;
      ModifyOrderItem.stage.close();
    }
    ModifyOrderItem.stage.close();
  }
}
