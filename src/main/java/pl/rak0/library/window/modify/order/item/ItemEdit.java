package pl.rak0.library.window.modify.order.item;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.PriceAmountBook;

public class ItemEdit {

  public static Stage stage;
  public static Boolean changed=false;


  public Boolean edit(PriceAmountBook priceAmountBook) throws IOException {
    ItemEditController.priceAmountBook=priceAmountBook;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/editOrderItemWIndow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/setting.png"));
    stage.setTitle("Edit item");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return changed;
  }

}
