package pl.rak0.library.window.modify.order;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;

public class CorrectionOrder {

  public static Boolean check = false;
  public static Stage stage;

  public Boolean correctOrder(Order order) throws IOException {
    CorrectionOrderController.order = order;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/correctionOrderWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/correction.png"));
    stage.setTitle("Order correction");
    stage.setScene(new Scene(root));
    stage.setResizable(true);
    stage.showAndWait();
    return check;
  }

}
