package pl.rak0.library.window.modify.seller;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.BookSeller;

public class ModifySeller {

  public static Stage stage;
  public static Boolean check=false;

  public Boolean isModify(BookSeller bookSeller) throws IOException {
    ModifySellerController.bookSeller=bookSeller;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/modifySellerWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/setting.png"));
    stage.setTitle("Modify Seller");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
    return check;
  }

}
