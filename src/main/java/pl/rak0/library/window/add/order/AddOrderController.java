package pl.rak0.library.window.add.order;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.controlsfx.control.textfield.TextFields;
import pl.rak0.library.alert.AlertOk;
import pl.rak0.library.alert.AlertYesOrNo;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.BookSeller;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.PriceAmountBook;
import pl.rak0.library.window.add.order.item.AddItemToOrder;
import pl.rak0.library.window.add.seller.AddSeller;
import pl.rak0.library.window.modify.order.item.ModifyOrderItem;

public class AddOrderController {


  @FXML
  private TableView<OrderItemForCell> orderItemsTableView;

  @FXML
  private TableColumn<OrderItemForCell, String> nameColumn;

  @FXML
  private TableColumn<OrderItemForCell, String> amountColumn;

  @FXML
  private TableColumn<OrderItemForCell, Integer> priceEachColumn;

  @FXML
  private TableColumn<OrderItemForCell, Double> priceTotalColumn;

  @FXML
  private Label totalPriceLabel;

  @FXML
  private TextField sellerTextField;

  @FXML
  private Label totalAmountLabel;

  @FXML
  private DatePicker orderDate;

  @FXML
  private TextField orderName;

  @FXML
  private DatePicker plannedReceiveDate;

  @FXML
  private Button sellerAddButton;

  private ObservableList<OrderItemForCell> allItems = FXCollections.observableArrayList();

  private ObservableList<String> allSellerNames = FXCollections.observableArrayList();

  public void initialize() {
    orderItemsTableView.setPlaceholder(new Label("No items in order"));
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
    priceEachColumn.setCellValueFactory(new PropertyValueFactory<>("priceEach"));
    priceTotalColumn.setCellValueFactory(new PropertyValueFactory<>("priceTotal"));
    orderDate.setValue(LocalDate.now());
    updateBookSeller();
    sellerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
      for (BookSeller bookSeller : Database.getAllItems(BookSeller.class)
          ) {
        if (bookSeller.getName().toLowerCase().equals(newValue.toLowerCase())) {
          sellerAddButton.setDisable(true);
          sellerAddButton.setDefaultButton(false);
        } else {
          sellerAddButton.setDisable(false);
          sellerAddButton.setDefaultButton(true);
        }
      }
    });
  }

  private void updateBookSeller() {
    for (BookSeller bookSeller : Database.getAllItems(BookSeller.class)
        ) {
      allSellerNames.add(bookSeller.getName());
    }
    TextFields.bindAutoCompletion(sellerTextField, allSellerNames);
  }

  @FXML
  private void addSeller() throws  IOException{
    AddSeller addSeller=new AddSeller();
    BookSeller bookSeller=addSeller.addSeller();
    if (bookSeller!=null){
      updateBookSeller();
      sellerTextField.setText(bookSeller.getName());
    }
  }

  private void updateTableAndLabels() {
    orderItemsTableView.setItems(allItems);
    Double totalPrize = 0.0;
    Integer totalAmount = 0;
    for (OrderItemForCell item : orderItemsTableView.getItems()
        ) {
      totalPrize += item.getPriceTotal();
      totalAmount += item.getAmount();
    }
    totalPriceLabel.setText("Price: " + totalPrize);
    totalAmountLabel.setText("Total Amount: " + totalAmount);
  }

  @FXML
  public void addItemToOrder() throws IOException {
    AddItemToOrder addItem = new AddItemToOrder();
    OrderItemForCell itemForCell = addItem.newItem();
    if (itemForCell != null) {
      allItems.add(itemForCell);
      updateTableAndLabels();
    } else {
      System.out.println("No item to add");
    }
  }

  @FXML
  public void modifyItemButton() throws Exception {
    if (!orderItemsTableView.getSelectionModel().isEmpty()) {
      OrderItemForCell orderItemForCell = new ModifyOrderItem()
          .isModify(orderItemsTableView.getSelectionModel().getSelectedItem());
      orderItemsTableView.getSelectionModel().getSelectedItem()
          .setAmount(orderItemForCell.getAmount());
      orderItemsTableView.getSelectionModel().getSelectedItem()
          .setPriceEach(orderItemForCell.getPriceEach());
      orderItemsTableView.getSelectionModel().getSelectedItem()
          .setUniqueBook(orderItemForCell.getUniqueBook().getId());
      orderItemsTableView.refresh();
    }
  }

  @FXML
  public void deleteItemButton() {
    if (orderItemsTableView.getSelectionModel().getSelectedItem() != null) {
      if (AlertYesOrNo.display("Delete book", "Delete for sure?")) {
        try {
          orderItemsTableView.getItems()
              .remove(orderItemsTableView.getSelectionModel().getSelectedItem());
        } catch (NullPointerException e) {
          System.out.println("Nothing picked up");
        }
      }
    }
  }

  @FXML
  public void saveOrderButton() {
    boolean findSeller = false;
    if (orderName.toString().isEmpty() || orderDate.getValue() == null
        || plannedReceiveDate.getValue() == null || sellerTextField.toString().isEmpty()
        || orderItemsTableView.getItems().isEmpty()) {
      for (BookSeller seller : Database.getAllItems(BookSeller.class)
          ) {
        if (seller.getName().equals(sellerTextField.getText())) {
          findSeller = true;
        }
      }
      if (!findSeller) {
        new AlertOk()
            .display("Error", "You need fill names dates, items and Seller must be in Database");
      } else {
        new AlertOk().display("Error", "You need fill names dates and items");
      }
    } else {
      try {

        ObservableList<PriceAmountBook> orderItems = FXCollections.observableArrayList();
        Order order = new Order();
        order.setSeller(Database.getSingleItem(BookSeller.class,
            Database.getBookSellerIdByName(sellerTextField.getText())));
        LocalDate localDate = orderDate.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date date = Date.from(instant);
        order.setCreateDate(date);
        order.setName(orderName.getText());
        LocalDate localDate1 = plannedReceiveDate.getValue();
        Instant instant1 = Instant.from(localDate1.atStartOfDay(ZoneId.systemDefault()));
        Date date1 = Date.from(instant1);
        order.setPlannedReceivedDate(date1);
        order.setReceivedDate(null);
        order.setComment("");
        for (OrderItemForCell item : orderItemsTableView.getItems()
            ) {
          PriceAmountBook priceAmountBook = new PriceAmountBook();
          priceAmountBook.setAmount(item.getAmount());
          priceAmountBook.setPrice(item.getPriceEach());
          priceAmountBook.setUniqueBook(item.getUniqueBook());
          Database.entityPersist(priceAmountBook);
          orderItems.add(priceAmountBook);
        }
        order.setPriceAmountBook(orderItems);
        Database.entityPersist(order);
        AddOrder.done=true;
        AddOrder.stage.close();
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }
  }
}
