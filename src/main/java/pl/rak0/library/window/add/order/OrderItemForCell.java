package pl.rak0.library.window.add.order;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.UniqueBook;

public class OrderItemForCell {

  private UniqueBook uniqueBook;
  private Integer amount;
  private Double priceEach;
  private Double priceTotal;

  public UniqueBook getUniqueBook() {
    return uniqueBook;
  }

  public void setUniqueBook(Integer id) {
    this.uniqueBook = Database.getSingleItem(UniqueBook.class, id);
  }

  public String getName() {
    return uniqueBook.getName();
  }

  public Integer getAmount() {
    return amount;
  }

  public String getType(){return  uniqueBook.getType();}

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Double getPriceEach() {
    return priceEach;
  }

  public void setPriceEach(Double prizeEach) {
    this.priceEach = prizeEach;
  }

  public Double getPriceTotal() {
    DecimalFormat df = new DecimalFormat("#.####");
    df.setRoundingMode(RoundingMode.CEILING);
    priceTotal=amount * priceEach;
    return Double.parseDouble(df.format(amount * priceEach));
  }

}
