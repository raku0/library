package pl.rak0.library.window.add.seller;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.BookSeller;

public class AddSeller {

  public static Stage stage;
  public static BookSeller bookSeller=null;

  public BookSeller addSeller() throws IOException {
    stage = new Stage();
    Parent root = FXMLLoader.load(getClass().getResource("/window/addBookSellerWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/man.png"));
    stage.setTitle("New Book Seller");
    stage.setScene(new Scene(root));
    stage.setMinWidth(100);
    stage.setMinHeight(150);
    stage.setResizable(false);
    stage.showAndWait();
    return bookSeller;
  }
}
