package pl.rak0.library.window.add.book;

import java.util.ArrayList;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.controlsfx.control.textfield.TextFields;
import pl.rak0.library.alert.AlertOk;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Book;
import pl.rak0.library.h2database.tables.UniqueBook;
import pl.rak0.library.utils.Genre;
import pl.rak0.library.utils.InputUtils;

public class AddBookController {

  @FXML
  private ComboBox typeBook;

  @FXML
  private TextField releaseDateTextField;

  @FXML
  private TextField nameTextField;

  @FXML
  private TextField authorTextField;

  @FXML
  private TextField descriptionTextField;

  @FXML
  private TextField numberOfPagesTextField;

  @FXML
  private TextField availableTextField;

  @FXML
  private Button cancelButton;


  public void initialize() {
    cancelButton.setOnAction(e -> AddBook.stage.close());
    typeBook.setItems(Genre.TYPES_OF_BOOKS);
    TextFields.bindAutoCompletion(nameTextField, Database.getAllNames()).setOnAutoCompleted(e -> {
      UniqueBook uniqueBook = Database
          .getSingleItem(UniqueBook.class, Database.getUniqueBookIdByName(nameTextField.getText()));
      authorTextField.setText(uniqueBook.getAuthor());
      releaseDateTextField.setText(uniqueBook.getReleaseDate().toString());
      numberOfPagesTextField.setText(uniqueBook.getNumberOfPages().toString());
      descriptionTextField.setText(uniqueBook.getDescription());
      typeBook.getSelectionModel().select(uniqueBook.getType());
    });
    TextFields.bindAutoCompletion(authorTextField, Database.getAllAuthor());
    InputUtils.addKeyTypingFilter(
        releaseDateTextField,
        event -> releaseDateTextField.getLength() < 4,
        event -> event.getCharacter().matches("\\d")
    );
    InputUtils.addKeyTypingFilter(
        numberOfPagesTextField,
        event -> numberOfPagesTextField.getLength() < 6,
        event -> event.getCharacter().matches("\\d")
    );
    InputUtils.addKeyTypingFilter(
        availableTextField,
        event -> availableTextField.getLength() < 2,
        event -> event.getCharacter().matches("\\d")
    );
  }

  @FXML
  private void createBook() {
    ArrayList<String> errors = new ArrayList<>();
    if (nameTextField.getText().isEmpty()) {
      errors.add("Name cannot be empty\n");
    }
    if (authorTextField.getText().isEmpty()) {
      authorTextField.setText("Unknown");
    }
    if (releaseDateTextField.getText().isEmpty()) {
      releaseDateTextField.setText("0");
    }
    if (numberOfPagesTextField.getText().isEmpty()) {
      errors.add("You need write amount of page\n");
    }
    if (descriptionTextField.getText().isEmpty()) {
      descriptionTextField.setText("No descriptions");
    }
    if (availableTextField.getText().isEmpty()) {
      availableTextField.setText("1");
    }
    if (typeBook.getSelectionModel().isEmpty()) {
      errors.add("Pick type of book\n");
    }
    if (errors.isEmpty()) {
      UniqueBook uniqueBook = new UniqueBook();
      uniqueBook.setName(nameTextField.getText());
      uniqueBook.setDescription(descriptionTextField.getText());
      uniqueBook.setNumberOfPages(Integer.parseInt(numberOfPagesTextField.getText()));
      uniqueBook.setReleaseDate(Integer.parseInt(releaseDateTextField.getText()));
      uniqueBook.setAuthor(authorTextField.getText());
      uniqueBook.setType(typeBook.getValue().toString());
      Book book = new Book(uniqueBook, Integer.parseInt(availableTextField.getText()));
      try {
        Database.entityPersist(book);
        AddBook.done = true;
        AddBook.stage.close();
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    } else {
      String errorMessage = errors.stream().collect(Collectors.joining());
      AlertOk.display("Errors", errorMessage);
    }
  }
}
