package pl.rak0.library.window.add.book;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddBook {

  public static Stage stage;
  public static boolean done = false;

  public boolean createBook() throws Exception {
    stage = new Stage();
    Parent root = FXMLLoader.load(getClass().getResource("/window/addBookWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/plus.png"));
    stage.setTitle("Add book");
    stage.setScene(new Scene(root));
    stage.setMinWidth(280);
    stage.setMinHeight(350);
    stage.setResizable(false);
    stage.showAndWait();
    return done;
  }
}