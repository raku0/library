package pl.rak0.library.window.add.seller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.BookSeller;

public class AddSellerController {

  @FXML
  private TextField nameSeller,companySeller;


  @FXML
  public void addSeller(){
    BookSeller bookSeller=new BookSeller();
    bookSeller.setName(nameSeller.getText());
    bookSeller.setCompany(companySeller.getText());
    Database.entityPersist(bookSeller);
    AddSeller.bookSeller=bookSeller;
    AddSeller.stage.close();
  }
  @FXML
  public void cancelSeller(){
    AddSeller.stage.close();
  }

}
