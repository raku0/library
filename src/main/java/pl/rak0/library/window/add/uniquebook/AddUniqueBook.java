package pl.rak0.library.window.add.uniquebook;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddUniqueBook {

  public static Stage stage;
  public static boolean done = false;

  public boolean createUniqueBook() throws Exception {
    stage = new Stage();
    Parent root = FXMLLoader.load(getClass().getResource("/window/addUniqueBookWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/plus.png"));
    stage.setTitle("Add book");
    stage.setScene(new Scene(root));
    stage.setMinWidth(280);
    stage.setMinHeight(300);
    stage.setResizable(false);
    stage.showAndWait();
    return done;
  }

}
