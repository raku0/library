package pl.rak0.library.window.add.order.item;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import org.controlsfx.control.textfield.TextFields;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.UniqueBook;
import pl.rak0.library.utils.InputUtils;
import pl.rak0.library.window.add.order.OrderItemForCell;
import pl.rak0.library.window.add.uniquebook.AddUniqueBook;


public class AddItemToOrderController {

  @FXML
  RowConstraints errorRow;

  @FXML
  GridPane gridPane;

  @FXML
  Button cancelButton;

  @FXML
  Button createBookButton;

  @FXML
  TextField nameTextField;

  @FXML
  TextField priceTextField;

  @FXML
  TextField amountTextField;

  @FXML
  TextField authorTextField;

  @FXML
  TextField typeTextField;

  @FXML
  Label errorMessage;


  public void initialize() {
    setAutoComplete();
    cancelButton.setOnAction(e -> AddItemToOrder.stage.close());
    nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
      if (Database.getAllNames().toString().toLowerCase().contains(newValue.toLowerCase())) {
        hideError();
      } else {
        showError();
      }
    });
  }

  private void setAutoComplete() {
    TextFields.bindAutoCompletion(nameTextField, Database.getAllNames())
        .setOnAutoCompleted(
            event -> {
              nameTextField.setEditable(false);
              UniqueBook uniqueBook = Database.getSingleItem(UniqueBook.class,
                  Database.getUniqueBookIdByName(nameTextField.getText()));
              authorTextField.setText(uniqueBook.getAuthor());
              typeTextField.setText(uniqueBook.getType());
            });
    InputUtils.addKeyTypingFilter(
        priceTextField,
        keyEvent -> keyEvent.getCharacter().matches("[0-9.]")
    );
    InputUtils.addKeyTypingFilter(
        amountTextField,
        keyEvent -> amountTextField.getLength() < 4,
        keyEvent -> keyEvent.getCharacter().matches("\\d")
    );
  }

  private void hideError() {
    createBookButton.setVisible(false);
    errorMessage.setVisible(false);
    errorRow.setMaxHeight(0);
    errorRow.setMinHeight(0);
  }

  private void showError() {
    errorRow.setMaxHeight(70);
    errorRow.setMinHeight(30);
    createBookButton.setVisible(true);
    errorMessage.setVisible(true);
    createBookButton.setOnAction(event -> {
      try {
        if (new AddUniqueBook().createUniqueBook()) {
          hideError();
          setAutoComplete();
          nameTextField.setText("");
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
  }

  @FXML
  public void checkAdd() {
    if (nameTextField.getText().isEmpty() || amountTextField.getText().isEmpty() || priceTextField
        .getText().isEmpty()) {
      showError();
    } else {
      OrderItemForCell orderItemForCell = new OrderItemForCell();
      orderItemForCell.setUniqueBook(Database.getUniqueBookIdByName(nameTextField.getText()));
      orderItemForCell.setAmount(Integer.parseInt(amountTextField.getText()));
      orderItemForCell.setPriceEach(Double.parseDouble(priceTextField.getText()));
      AddItemToOrder.orderItemForCell = orderItemForCell;
      AddItemToOrder.stage.close();
    }
  }
}
