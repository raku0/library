package pl.rak0.library.window.add.order.item;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.window.add.order.OrderItemForCell;

public class AddItemToOrder {

  public static Stage stage;
  public static OrderItemForCell orderItemForCell = null;

  public OrderItemForCell newItem() throws IOException {
    stage = new Stage();
    Parent root = FXMLLoader.load(getClass().getResource("/window/addItemToOrderWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/plus.png"));
    stage.setTitle("Add Item To Order");
    stage.setScene(new Scene(root));
    stage.setMinWidth(280);
    stage.setMinHeight(350);
    stage.setResizable(false);
    stage.showAndWait();
    OrderItemForCell toReturn = orderItemForCell;
    orderItemForCell = null;
    return toReturn;
  }
}
