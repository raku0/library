package pl.rak0.library.window.information.book;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.rak0.library.h2database.tables.Book;


public class InformationController {

  public static Book book;
  public static boolean informationView = true;

  @FXML
  public TextField authorTextField;

  @FXML
  public TextField releaseDateTextField;

  @FXML
  public TextField numberOfPageTextField;

  @FXML
  public TextField availableTextField;

  @FXML
  public TextField nameTextField;

  @FXML
  public TextField rentTextField;

  @FXML
  public TextField rentDateTextField;

  @FXML
  public TextArea descriptionTextArea;

  @FXML
  public Button okButton;

  @FXML
  public TextField typeTextField;

  public void initialize() {
    okButton.setOnAction(e -> InformationBook.stage.close());
    nameTextField.setText(book.getName());
    typeTextField.setText(book.getType());
    authorTextField.setText(book.getAuthor());
    releaseDateTextField.setText(String.valueOf(book.getReleaseDate()).equals("0") ? "Unknown"
        : String.valueOf(book.getReleaseDate()));
    rentTextField.setText(book.isRent() ? "Yes" : "No");
    rentDateTextField.setText(book.getRentDate() == null ? "" : book.getRentDate().toString());
    numberOfPageTextField.setText(String.valueOf(book.getNumberOfPages()));
    availableTextField.setText(String.valueOf(book.getAvailable()));
    descriptionTextArea.setText(book.getDescription());
  }
}
