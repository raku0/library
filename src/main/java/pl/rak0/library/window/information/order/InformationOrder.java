package pl.rak0.library.window.information.order;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;

public class InformationOrder {

  public InformationOrder(Order order) throws IOException {
    InformationOrderController.order=order;
    Stage stage=new Stage();
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/information.png"));
    Parent root = FXMLLoader.load(getClass().getResource("/window/informationOrderWindow.fxml"));
    stage.setTitle("Order information");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
  }
}
