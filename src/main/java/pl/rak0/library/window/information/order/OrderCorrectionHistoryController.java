package pl.rak0.library.window.information.order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.OrderCorrectionHistory;

public class OrderCorrectionHistoryController {
  public static Order order;

  @FXML
  public TableView<OrderCorrectionHistory>correctionHistory;

  @FXML
  public TableColumn<OrderCorrectionHistory,String>reasonChange,changeDate;

  public void initialize(){
    correctionHistory.setPlaceholder(new Label("No corrections"));
    reasonChange.setCellValueFactory(new PropertyValueFactory<>("reason"));
    changeDate.setCellValueFactory(new PropertyValueFactory<>("changeDate"));
    changeDate.setCellValueFactory(e->{
      OrderCorrectionHistory order=e.getValue();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      return new SimpleStringProperty(dateFormat.format(order.getChangeDate()));
    });
    correctionHistory.getItems().addAll(order.getOrderCorrectionHistories());
  }

  @FXML
  public void information(){
    //TODO
  }

  @FXML
  public void exit(){
    pl.rak0.library.window.information.order.OrderCorrectionHistory.stage.close();
  }

}
