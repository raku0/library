package pl.rak0.library.window.information.book;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Book;

public class InformationBook {

  public static Stage stage;

  public InformationBook(Book book) throws Exception {
    InformationController.book = book;
    InformationController.informationView = true;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/informationBookWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/information.png"));
    stage.setTitle("Information");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();

  }
}
