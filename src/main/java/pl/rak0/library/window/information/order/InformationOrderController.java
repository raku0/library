package pl.rak0.library.window.information.order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.h2database.Database;
import pl.rak0.library.h2database.tables.Book;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.PriceAmountBook;
import pl.rak0.library.h2database.tables.UniqueBook;

public class InformationOrderController {

  public static Order order;

  @FXML
  private Label nameLabel, sellerLabel, createDateLabel, amountLabel, totalPriceLabel,
      plannedReceivedDateLabel, receivedDateLabel, commentLabel;
  @FXML
  private TableView<PriceAmountBook> orderItemView;
  @FXML
  private TableView<Order> sellerHistoryTableView;
  @FXML
  private TableColumn<Order,String>nameSeller;
  @FXML
  private TableColumn<Order,Integer>amountBook;
  @FXML
  private TableColumn<Order,String>receivedDate;
  @FXML
  private TableColumn<PriceAmountBook, String> nameColumn, authorColumn;
  @FXML
  private TableColumn<PriceAmountBook, Double> priceColumn, totalPriceColumn;
  @FXML
  private TableColumn<PriceAmountBook, Integer> amountColumn;

  private ObservableList<PriceAmountBook> allItems = FXCollections.observableArrayList();


  public void initialize() {
    allItems.addAll(order.getPriceAmountBook());
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
    priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
    amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
    totalPriceColumn.setCellValueFactory(new PropertyValueFactory<>("priceTotal"));
    orderItemView.setItems(allItems);
    totalPriceLabel.setText(totalPriceLabel.getText() + order.getTotalPrice());
    amountLabel.setText(amountLabel.getText() + order.getTotalAmount());
    nameLabel.setText(nameLabel.getText() + order.getName());
    sellerLabel.setText(sellerLabel.getText() + order.getSeller().getName());
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    createDateLabel.setText(createDateLabel.getText() + sdf.format(order.getCreateDate()));
    plannedReceivedDateLabel
        .setText(plannedReceivedDateLabel.getText() + sdf.format(order.getPlannedReceivedDate()));
    if (order.getComment() != null) {
      commentLabel.setText(commentLabel.getText() + order.getComment());
    } else {
      commentLabel.setText(commentLabel.getText() + "No comment");
    }
    if (order.getReceivedDate() != null) {
      receivedDateLabel.setText(receivedDateLabel.getText() + sdf.format(order.getReceivedDate()));
    } else {
      receivedDateLabel.setText(receivedDateLabel.getText() + "Not yet");
    }
    nameSeller.setCellValueFactory(new PropertyValueFactory<>("sellerName"));
    amountBook.setCellValueFactory(new PropertyValueFactory<>("totalAmount"));
    receivedDate.setCellValueFactory(new PropertyValueFactory<>("receivedDate"));
    receivedDate.setCellValueFactory(string->{
      Order order=string.getValue();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      return new SimpleStringProperty(dateFormat.format(order.getPlannedReceivedDate()));
    });

    sellerHistoryTableView.setPlaceholder(new Label("No history of Seller"));
    ObservableList<Order> allOrder=Database.getAllItems(Order.class);
    ObservableList<Order> filteredOrder=FXCollections.observableArrayList();
    for (Order order2:allOrder
    ) {
      if (order2.getSeller().getId().equals(order.getSeller().getId())&&order2.getReceivedDate()!=null){
        filteredOrder.add(order2);
      }
    }
    sellerHistoryTableView.setItems(filteredOrder);


  }
}
