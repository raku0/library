package pl.rak0.library.window.information.order;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.rak0.library.h2database.tables.Order;
import pl.rak0.library.h2database.tables.PriceAmountBook;

public class HistoryOrderInformationController {

  public static Order order;

  @FXML
  private TableView<PriceAmountBook> orderItemTableView;

  @FXML
  private TableColumn<PriceAmountBook, String> nameColumn, authorColumn, receivedColumn;

  @FXML
  private TableColumn<PriceAmountBook, Integer> amountColumn;

  @FXML
  private TableColumn<PriceAmountBook, Double> priceColumn;

  @FXML
  private Label nameLabel, sellerNameLabel, receivedDateLabel, plannedReceivedDateLabel, createDateLabel,
      commentLabel;

  public void initialize() {
    nameLabel.setText(nameLabel.getText() + order.getName());
    sellerNameLabel.setText(sellerNameLabel.getText() + order.getSellerName());
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    receivedDateLabel
        .setText(receivedDateLabel.getText() + dateFormat.format(order.getReceivedDate()));
    plannedReceivedDateLabel
        .setText(
            plannedReceivedDateLabel.getText() + dateFormat.format(order.getPlannedReceivedDate()));
    createDateLabel.setText(createDateLabel.getText() + dateFormat.format(order.getCreateDate()));
    if (order.getComment() == null || order.getComment().equals("")) {
      commentLabel.setText(commentLabel.getText() + "No comment");
    }
    else{commentLabel.setText(commentLabel.getText() + order.getComment());}
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
    receivedColumn.setCellValueFactory(new PropertyValueFactory<>("received"));
    receivedColumn.setCellValueFactory(s -> {
      String value;
      if (s.getValue().getReceived() != null) {
        value = s.getValue().getReceived() ? "Yes" : "No";
      } else {
        value = "No";
      }
      return new SimpleStringProperty(value);
    });

    amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
    priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
    ObservableList<PriceAmountBook> allItems = FXCollections.observableArrayList();
    allItems.addAll(order.getPriceAmountBook());
    orderItemTableView.setItems(allItems);
  }
  @FXML
  public void showHistory() throws IOException {
    OrderCorrectionHistory orderCorrectionHistory=new OrderCorrectionHistory();
    orderCorrectionHistory.showHistory(order);
  }
}
