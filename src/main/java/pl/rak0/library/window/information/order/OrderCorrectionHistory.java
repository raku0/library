package pl.rak0.library.window.information.order;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.rak0.library.h2database.tables.Order;

public class OrderCorrectionHistory {
  public static Stage stage;

  public void showHistory(Order order) throws IOException {
    OrderCorrectionHistoryController.order=order;
    stage = new Stage();
    Parent root = FXMLLoader
        .load(getClass().getResource("/window/orderCorrectionHistoryWindow.fxml"));
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.getIcons().add(new Image("/icons/information.png"));
    stage.setTitle("Order correction");
    stage.setScene(new Scene(root));
    stage.setResizable(false);
    stage.showAndWait();
  }

}
