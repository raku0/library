package pl.rak0.library;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;

public class Controller implements Initializable {

  @FXML
  private GridPane mainContent;

  @FXML
  private void booksPane() throws IOException {
    mainContent.getChildren().clear();
    mainContent.getChildren().add(FXMLLoader.load(getClass().getResource("/view/booksView.fxml")));
  }

  @FXML
  private void analystPane() throws IOException {
    mainContent.getChildren().clear();
    mainContent.getChildren().add(FXMLLoader.load(getClass().getResource("/view/booksView.fxml")));
  }

  @FXML
  private void ordersPane() throws IOException {
    mainContent.getChildren().clear();
    mainContent.getChildren().add(FXMLLoader.load(getClass().getResource(
        "/view/orderView.fxml")));
  }

  @FXML
  private void statisticsPane() throws IOException {
    mainContent.getChildren().clear();
    mainContent.getChildren().add(FXMLLoader.load(getClass().getResource("/view/booksView.fxml")));
  }

  @FXML
  private void usersPane() throws IOException {
    mainContent.getChildren().clear();
    mainContent.getChildren().add(FXMLLoader.load(getClass().getResource("/view/booksView.fxml")));
  }

  @FXML
  private void settingsPane() throws IOException {
    mainContent.getChildren().clear();
    mainContent.getChildren().add(FXMLLoader.load(getClass().getResource("/view/booksView.fxml")));
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    try {
      mainContent.getChildren()
          .add(FXMLLoader.load(getClass().getResource("/view/booksView.fxml")));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
