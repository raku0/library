package pl.rak0.library.utils;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyEvent;

public class InputUtils {

  @SafeVarargs
  public static void addKeyTypingFilter(TextInputControl input,
      Function<KeyEvent, Boolean>... filters) {
    Arrays.stream(filters)
        .map(InputUtils::toKeyTypedFilter)
        .forEach(filter -> input.addEventFilter(KeyEvent.KEY_TYPED, filter));
  }

  private static EventHandler<KeyEvent> toKeyTypedFilter(Function<KeyEvent, Boolean> filter) {
    return event -> Optional.ofNullable(filter.apply(event))
        .filter(filtered -> !filtered)
        .ifPresent(notPassed -> event.consume());
  }
}
