package pl.rak0.library.utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Genre {

  public static final ObservableList<String> TYPES_OF_BOOKS = FXCollections.observableArrayList(
      "Adventure",
      "Science fiction",
      "Drama",
      "Action and Adventure",
      "Romance",
      "Mystery",
      "Horror",
      "Guide",
      "Travel",
      "Children's books",
      "Religion",
      "Science",
      "History",
      "Poetry",
      "Comics",
      "Art",
      "Cookbooks",
      "Series",
      "Biographies",
      "Fantasy",
      "Anime"
  );
}
