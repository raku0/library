package pl.rak0.library.alert;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertYesOrNo {

  private static boolean choice = false;

  public static boolean display(String title, String message) {
    Stage window = new Stage();
    window.getIcons().add(new Image("/icons/delete.png"));
    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle(title);
    window.setMinWidth(250);
    window.setWidth(250);
    window.setMaxWidth(350);
    window.setHeight(200);
    window.setMinHeight(150);
    window.setMaxHeight(350);
    window.setResizable(false);
    GridPane layout = new GridPane();
    layout.setAlignment(Pos.CENTER);
    layout.setVgap(10);
    layout.setPadding(new Insets(15, 15, 15, 15));

    Label label = new Label(message);
    label.setFont(Font.font(20));
    label.setWrapText(true);
    layout.add(label, 0, 0);

    HBox hBox = new HBox();
    hBox.setSpacing(35);
    hBox.setAlignment(Pos.BOTTOM_CENTER);
    layout.add(hBox, 0, 1);
    Image imageYes = new Image("/icons/yes.png");
    Button yes = new Button("Yes", new ImageView(imageYes));
    yes.setMinWidth(30);
    yes.setMinHeight(30);
    yes.setAlignment(Pos.CENTER);
    yes.setOnAction(e -> {
      choice = true;
      window.close();
    });
    Image imageNo = new Image("/icons/cancel.png");
    Button no = new Button("No", new ImageView(imageNo));
    no.setMinWidth(30);
    no.setMinHeight(30);
    no.setAlignment(Pos.CENTER);
    no.setOnAction(e -> {
      choice = false;
      window.close();
    });
    hBox.getChildren().addAll(yes, no);

    Scene scene = new Scene(layout);
    window.setScene(scene);
    window.showAndWait();

    return choice;


  }

}
