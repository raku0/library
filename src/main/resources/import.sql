INSERT INTO UNIQUEBOOKS(AUTHOR,DESCRIPTION,NAME,NUMBEROFPAGES,RELEASEDATE,TYPE) VALUES ('Peter','Description','Magic and Swords',600,1980,'Mystery');
INSERT INTO UNIQUEBOOKS(AUTHOR,DESCRIPTION,NAME,NUMBEROFPAGES,RELEASEDATE,TYPE) VALUES ('Gilbert Adair','Long description long long','Killer One',200,1980,'Horror');
INSERT INTO UNIQUEBOOKS(AUTHOR,DESCRIPTION,NAME,NUMBEROFPAGES,RELEASEDATE,TYPE) VALUES ('Lascelles Abercrombie','Short description','Alice Adventure',200,1980,'Adventure');
INSERT INTO BOOKS(uniquebook_id,AVAILABLE,RENT) VALUES(1,1,FALSE );
INSERT INTO BOOKS(uniquebook_id,AVAILABLE,RENT) VALUES(2,2,FALSE );
INSERT INTO BOOKS(uniquebook_id,AVAILABLE,RENT) VALUES(3,3,FALSE );
INSERT INTO SELLERBOOKS(NAME,COMPANY) VALUES ('Sample Big Seller','Test Big Company');
INSERT INTO SELLERBOOKS(NAME,COMPANY) VALUES ('Sample Seller','Test Seller Company');
INSERT INTO ORDERS(NAME,CREATEDATE,COMMENT,SELLER_ID,plannedReceivedDate) VALUES ('Test1','2018-08-30','',1,'2018-09-23');
INSERT INTO PRICE_AMOUNT(AMOUNT,PRICE,UNIQUEBOOK_ID)VALUES(3,10.0,3);
INSERT INTO PRICE_AMOUNT(AMOUNT,PRICE,UNIQUEBOOK_ID)VALUES(2,12.0,1);
INSERT INTO ORDERS_PRICE_AMOUNT(ORDERS_ID,PRICEAMOUNTBOOK_ID)VALUES(1,1);
INSERT INTO ORDERS_PRICE_AMOUNT(ORDERS_ID,PRICEAMOUNTBOOK_ID)VALUES(1,2);
INSERT INTO ORDERS(NAME,CREATEDATE,COMMENT,SELLER_ID,plannedReceivedDate,ReceivedDate) VALUES ('Test Received','2018-06-05','',1,'2018-09-23','2018-09-23');
INSERT INTO PRICE_AMOUNT(AMOUNT,PRICE,UNIQUEBOOK_ID)VALUES(7,3.0,3);
INSERT INTO PRICE_AMOUNT(AMOUNT,PRICE,UNIQUEBOOK_ID)VALUES(5,9.0,1);
INSERT INTO ORDERS_PRICE_AMOUNT(ORDERS_ID,PRICEAMOUNTBOOK_ID)VALUES(2,3);
INSERT INTO ORDERS_PRICE_AMOUNT(ORDERS_ID,PRICEAMOUNTBOOK_ID)VALUES(2,4);